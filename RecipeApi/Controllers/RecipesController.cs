﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using RecipeApi.Models;

namespace RecipeApi.Controllers
{
    [Route("api/[controller]")]
    public class RecipesController : Controller
    {
        IDictionary<string, Recipe> recipes = new Dictionary<string, Recipe>();
        private readonly IRecipeStore recipeStore;

        public RecipesController(IRecipeStore recipeStore)
        {
            this.recipeStore = recipeStore;
        }

        // GET api/recipes
        [HttpGet]
        public IEnumerable<Recipe> Get()
        {
            return recipeStore.GetAll();
        }

        // GET api/recipes/5
        [HttpGet("{id}")]
        public Recipe Get(int id)
        {
            return recipeStore.Get(id);
        }

        // POST api/recipes
        [HttpPost]
        public void Post([FromBody]Recipe recipe)
        {
            recipeStore.Add(recipe);
        }

        // PUT api/recipes/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]Recipe recipe)
        {
            recipeStore.Update(id, recipe);
        }

        // DELETE api/recipes/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            recipeStore.Delete(id);
        }
    }

    public class RecipeStore : IRecipeStore
    {
        IList<Recipe> recipes;
        public RecipeStore()
        {
            recipes = new List<Recipe>();
        }
        public void Add(Recipe recipe)
        {
            recipes.Add(recipe);
        }

        public void Delete(int id)
        {
            recipes.RemoveAt(id);
        }

        public Recipe Get(int id)
        {
            if(id < recipes.Count)
                return recipes[id];
            return null;
        }

        public IEnumerable<Recipe> GetAll()
        {
            return recipes;
        }

        public void Update(int id, Recipe recipe)
        {
            recipes[id] = recipe;
        }
    }

    public interface IRecipeStore
    {
        IEnumerable<Recipe> GetAll();
        Recipe Get(int id);
        void Add(Recipe recipe);
        void Update(int id, Recipe recipe);
        void Delete(int id);
    }
}
