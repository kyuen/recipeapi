using Moq;
using RecipeApi.Controllers;
using RecipeApi.Models;
using System.Collections.Generic;
using Xunit;

namespace RecipeApi.Test
{
    public class RecipesControllerTest
    {
        [Fact]
        public void When_requesting_all_recipes_returns_collection_of_recipes_from_recipe_store()
        {
            var mockStore = new Mock<IRecipeStore>();
            var expected = new List<Recipe> { new Recipe() };
            mockStore.Setup(store => store.GetAll()).Returns(expected);
            var controller = new RecipesController(mockStore.Object);
            var result = controller.Get();
            Assert.IsAssignableFrom<IEnumerable<Recipe>>(result);
            Assert.Same(expected, result);
        }

        [Fact]
        public void When_requesting_a_single_recipe_using_the_id_returns_corresponding_recipe()
        {
            var mockStore = new Mock<IRecipeStore>();
            var expected = new Recipe();
            mockStore.Setup(store => store.Get(0)).Returns(expected);
            var controller = new RecipesController(mockStore.Object);
            var result = controller.Get(0);
            Assert.IsType<Recipe>(result);
            Assert.Same(expected, result);
        }

        [Fact]
        public void When_putting_specific_recipe_it_uses_recipe_store_to_update_that_recipe()
        {
            var mockStore = new Mock<IRecipeStore>();
            var expected = new Recipe();
            var controller = new RecipesController(mockStore.Object);
            controller.Put(0, expected);
            mockStore.Verify(store => store.Update(0, expected), Times.Once);
        }

        [Fact]
        public void When_posting_recipe_it_uses_recipe_store_to_add_that_recipe()
        {
            var mockStore = new Mock<IRecipeStore>();
            var expected = new Recipe();
            var controller = new RecipesController(mockStore.Object);
            controller.Post(expected);
            mockStore.Verify(store => store.Add(expected), Times.Once);
        }

        [Fact]
        public void When_deleting_recipe_by_id_it_uses_recipe_store_to_delete_that_recipe()
        {
            var mockStore = new Mock<IRecipeStore>();
            var controller = new RecipesController(mockStore.Object);
            controller.Delete(0);
            mockStore.Verify(store => store.Delete(0));
        }
    }
}
